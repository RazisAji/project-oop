<?php
require_once "elang.php";
require_once "harimau.php";

echo "<h3> Battle </h3>";
echo "<h4> Player 1 </h4>";
$elang = new elang(1);
echo $elang->getInfoHewan();
echo $elang->atraksi();

echo "<h4> Player 2 </h4>";
$harimau = new harimau(1);
echo $harimau->getInfoHewan();
echo $harimau->atraksi();

echo "<h4> Fight! </h4>";
echo $harimau->serang($elang);
echo $elang->diserang($harimau);
echo $elang->serang($harimau);
echo $harimau->diserang($elang);


?>